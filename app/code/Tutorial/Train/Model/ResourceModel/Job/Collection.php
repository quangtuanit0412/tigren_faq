<?php
namespace Tutorial\Train\Model\ResourceModel\Job;
 
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
 
    protected $_idFieldName = \Tutorial\Train\Model\Job::JOB_ID;
 
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tutorial\Train\Model\Job', 'Tutorial\Train\Model\ResourceModel\Job');
    }
 
}