<?php
namespace Tutorial\Train\Model\ResourceModel\Department;
 
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
 
    protected $_idFieldName = \Tutorial\Train\Model\Department::DEPARTMENT_ID;
     
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tutorial\Train\Model\Department', 'Tutorial\Train\Model\ResourceModel\Department');
    }
 
}