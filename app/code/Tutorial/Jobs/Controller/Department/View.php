<?php
namespace Tutorial\Jobs\Controller\Department;
class View extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        // print_r($this->_view->getLayout()->getAllBlocks);die;

        $this->_view->renderLayout();
    }
}