<?php

namespace Tigren\Faq\Controller\Question;
/**
 * Class View
 * @package Tigren\Faq\Controller\Question
 */
class View extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Tigren\Faq\Model\Question $question
    )
    {
        $this->_question = $question;
        parent::__construct($context);
    }
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}