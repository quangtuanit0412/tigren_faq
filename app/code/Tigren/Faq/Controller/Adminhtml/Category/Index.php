<?php

namespace Tigren\Faq\Controller\Adminhtml\Category;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

/**
 * Class Index
 * @package Tigren\Faq\Controller\Adminhtml\Category
 */
class Index extends Action
{
    /**
     *
     */
    const ADMIN_RESOURCE = 'Tigren_Faq::category';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Tigren_Faq::category');
        $resultPage->addBreadcrumb(__('Faq'), __('Faq'));
        $resultPage->addBreadcrumb(__('Manage Category Question'), __('Manage Category Question'));
        $resultPage->getConfig()->getTitle()->prepend(__('Category'));

        return $resultPage;
    }
}