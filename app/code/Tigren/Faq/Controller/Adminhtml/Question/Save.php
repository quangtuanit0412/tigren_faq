<?php

namespace Tigren\Faq\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;

/**
 * Class Save
 * @package Tigren\Faq\Controller\Adminhtml\Question
 */
class Save extends Action
{

    /**
     * @var \Tigren\Faq\Model\Question
     */
    protected $_question;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param \Tigren\Faq\Model\Question $question
     */
    public function __construct(
        Action\Context $context,
        \Tigren\Faq\Model\Question $question
    )
    {
        parent::__construct($context);
        $this->_question = $question;
    }


    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Tigren_Faq::question_save');
    }


    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_question;
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Question saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the question'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}