<?php

namespace Tigren\Faq\Block\Question;
/**
 * Class View
 * @package Tigren\Faq\Block
 */
class View extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Tigren\Faq\Model\Category
     */
    protected $_category;
    /**
     * @var \Tigren\Faq\Model\Question
     */
    protected $_question;

    /**
     * View constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Tigren\Faq\Model\Category $category
     * @param \Tigren\Faq\Model\Question $question
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Tigren\Faq\Model\CategoryFactory $category,
        \Tigren\Faq\Model\QuestionFactory $question,
        array $data = []
    )
    {
        $this->_category = $category;
        $this->_question = $question;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return $this|\Magento\Framework\View\Element\Template
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        $title = __('FAQ Home Page');
        $this->getLayout()->createBlock('Magento\Catalog\Block\Breadcrumbs');

        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb(
                'job',
                [
                    'label' => $title,
                    'title' => $title,
                    'link' => false // No link for the last element
                ]
            );
        }
        return $this;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getFaqQuestion()
    {
        $questions = $this->_question->create();
        $collection = $questions->getCollection();
        if(!empty($this->getParamIdCategory())) {
            return $collection->addCategoryFilter($this->getParamIdCategory(),$questions);
        }
        return $collection;
    }

    /**
     * @return mixed
     */
    public function getFaqCategory()
    {
        $categorys = $this->_category->create();
        $collection = $categorys->getCollection();
        return $collection;
    }

    /**
     * @return string
     */
    public function getBackQuestion()
    {
        return $this->getUrl('faq/question/view');
    }

    /**
     * @param $id
     * @return string
     */
    public function getCategoryUrl($id)
    {
        return $this->getUrl('faq/question/view',['id'=>$id]);
    }

    /**
     * @return mixed
     */
    public function getParamIdCategory()
    {
        return $this->getRequest()->getParam('id');
    }
}