<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\Faq\Setup;

use Tigren\Faq\Model\Question;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var Question
     */
    protected $_question;

    /**
     * UpgradeData constructor.
     * @param Question $question
     */
    public function __construct(Question $question)
    {
        $this->_question = $question;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // Action to do if module version is less than 1.0.0.1
        if (version_compare($context->getVersion(), '1.0.0.0') < 0) {
            $questions = [
                [
                    'name' => 'Do you deliver orders to Canada?',
                    'full_answer' => 'Sed cautela nimia in peiores haeserat plagas, ut narrabimus postea,
                aemulis consarcinantibus insidias graves apud Constantium, cetera medium principem sed
                siquid auribus eius huius modi quivis infudisset ignotus, acerbum et inplacabilem et in
                hoc causarum titulo dissimilem sui.',
                    'status	' => 1
                ],
                [
                    'name' => 'How can I subscribe to discount notifications?',
                    'full_answer' => 'Please follow this link to get subscribed.',
                    'status	' => 1
                ],
                [
                    'name' => 'How do you like this product questions extension?',
                    'full_answer' => 'apud Constantium, cetera medium principem sed
                siquid auribus eius huius modi quivis infudisset ignotus',
                    'status	' => 1
                ]
            ];


            $questionsIds = array();
            foreach ($questions as $data) {
                $question = $this->_question->setData($data)->save();
                $questionsIds[] = $question->getId();
            }
        }

        $installer->endSetup();
    }
}