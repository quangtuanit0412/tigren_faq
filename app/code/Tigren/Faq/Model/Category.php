<?php

namespace Tigren\Faq\Model;

use \Magento\Framework\Model\AbstractModel;

/**
 * Class Category
 * @package Tigren\Faq\Model
 */
class Category extends AbstractModel
{
    /**
     *
     */
    const CATEGORY_ID = 'category_id'; // We define the id fieldname

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'faq'; // parent value is 'core_abstract'

    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'category'; // parent value is 'object'

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = self::CATEGORY_ID; // parent value is 'id'

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Faq\Model\ResourceModel\Category');
    }

    /**
     * @return int
     */
    protected function getEnableStatus()
    {
        return 1;
    }

    /**
     * @return int
     */
    protected function getDisableStatus()
    {
        return 0;
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [$this->getEnableStatus() => __('Enable'), $this->getDisableStatus() => __('Disable')];
    }
}