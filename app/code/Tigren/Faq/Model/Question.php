<?php

namespace Tigren\Faq\Model;

use \Magento\Framework\Model\AbstractModel;

/**
 * Class Question
 * @package Tigren\Faq\Model
 */
class Question extends AbstractModel
{
    /**
     *
     */
    const QUESTION_ID = 'entity_id'; // We define the id fieldname

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'faq'; // parent value is 'core_abstract'

    /**
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'question'; // parent value is 'object'

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = self::QUESTION_ID; // parent value is 'id'

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Faq\Model\ResourceModel\Question');
    }

    /**
     * @return int
     */
    protected function getPendingStatus()
    {
        return 1;
    }

    /**
     * @return int
     */
    public function getAnsweredStatus()
    {
        return 2;
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [$this->getPendingStatus() => __('Pending'), $this->getAnsweredStatus() => __('Answered')];
    }
}