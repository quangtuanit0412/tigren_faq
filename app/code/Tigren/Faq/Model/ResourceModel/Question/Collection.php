<?php

namespace Tigren\Faq\Model\ResourceModel\Question;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Collection
 * @package Tigren\Faq\Model\ResourceModel\Question
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = \Tigren\Faq\Model\Question::QUESTION_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Faq\Model\Question', 'Tigren\Faq\Model\ResourceModel\Question');
    }

    /**
     * @param $question
     * @param $category
     * @return $this
     */
    public function addStatusFilter($question, $category)
    {
        $this->addFieldToSelect('*')
            ->addFieldToFilter('status', array('eq' => $question->getAnsweredStatus()))
            ->join(
                ['category' => $category->getResource()->getMainTable()],
                'main_table.category_id = category.' . $category->getIdFieldName(),
                ['category_name' => 'category_name']
            );
        return $this;
    }
    public function addCategoryFilter($id,$question)
    {
        $this->addFieldToSelect('*')
            ->addFieldToFilter('status', array('eq' => $question->getAnsweredStatus()))
            ->addFieldToFilter('category_id',array('eq' => $id));
        return $this;
    }

}
