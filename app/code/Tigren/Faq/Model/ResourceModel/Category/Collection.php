<?php

namespace Tigren\Faq\Model\ResourceModel\Category;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Tigren\Faq\Model\ResourceModel\Category
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = \Tigren\Faq\Model\Category::CATEGORY_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Tigren\Faq\Model\Category', 'Tigren\Faq\Model\ResourceModel\Category');
    }

    /**
     *
     */
    public function getCategoryPageHome()
    {

    }
}