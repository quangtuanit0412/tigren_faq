<?php
namespace Tigren\Faq\Model\Source\Question;

/**
 * Class Status
 * @package Tigren\Faq\Model\Source\Question
 */
class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Tigren\Faq\Model\Question
     */
    protected $_question;

    /**
     * Status constructor.
     * @param \Tigren\Faq\Model\Question $question
     */
    public function __construct(\Tigren\Faq\Model\Question $question)
    {
        $this->_question = $question;
    }
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_question->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}