<?php
namespace Tigren\Faq\Model\Source;

/**
 * Class Category
 * @package Tigren\Faq\Model\Source
 */
class Category implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var \Tigren\Faq\Model\Category
     */
    protected $_category;


    /**
     * Category constructor.
     * @param \Tigren\Faq\Model\Category $category
     */
    public function __construct(\Tigren\Faq\Model\Category $category)
    {
        $this->_category = $category;
    }


    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $categoryCollection = $this->_category->getCollection()
            ->addFieldToSelect('category_id')
            ->addFieldToSelect('category_name');
        foreach ($categoryCollection as $category) {
            $options[] = [
                'value' => $category->getId(),
                'label' => $category->getCategoryName(),
            ];
        }
//        echo '<pre>'; print_r($options);die((__FILE__).'-->'.(__FUNCTION__).'--Line('. (__LINE__).')');
        return $options;
    }
}