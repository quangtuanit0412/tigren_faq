<?php

namespace Tigren\Faq\Model\Source\Category;

/**
 * Class Status
 * @package Tigren\Faq\Model\Source\Category
 */
class Status implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var \Tigren\Faq\Model\Category
     */
    protected $_category;

    /**
     * Status constructor.
     * @param \Tigren\Faq\Model\Category $category
     */
    public function __construct(\Tigren\Faq\Model\Category $category)
    {
        $this->_category = $category;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->_category->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}